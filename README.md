# Combined Assignment

---

### Assignment by:

- Abhishek Pathak (AS107)
- Chaitanya Prasad (AS055)

---

- There are four packages, all separately contains code for respective assignment
- There is an util package for utility class. even there is only few methods.
- proto resources are present in separate directory known as proto_resources.

---

## Assignment 1

### Problem statement:

Using hdfs and Hbase java api ⇒ Create 100 csv files having People information in it. Store them to HDFS and write a
java program to load these csv files from HDFS, read column values and load into HBASE Sample People info : Name, age,
company, building_code, phone_number, address

### Solution classes:

- **Main.java:** Driver class
- **HbaseUtils.java:** Util class for Hbase related methods
- **HDFSUtil.java:** Util class for HDFS related methods
- **Employee.java:** Helper class to generate 100 csv sample files

---

## Assignment 2

### Problem Statement

Create a MR job to read files from HDFS and count the frequency of each word in the file. Create a MR job to load data
from the above created HBASE people table and write it back to a new HBASE table using bulk upload.

### Solution Classes:

- WordCountMapReduceJob.java: This is a Map Reduce job to read files from HDFS and count the frequency of each word in
  the file, and store it in the Outfile specified.
- BulkImportMapReduceJob.java:
- HbaseBulkLoad.java:
- HbaseBulkLoadMapper.java:

---

## Assignment 3

### Problem Statement

Create a java program to read data from csv and then set it to proto objects and print them using these proto objects.
Proto files to be included :

|FileName| Details|
|--------|--------|
| Employee.proto | Example fields can be: name, employee_id, building_code, floor_number (this should be enum), salary, department etc |
| Building.proto | Example fields can be: building_code, total_floors, companies_in_the_building, cafteria_code |

### Solution Classes

- Main.java: Driver class
- ProtobufUtils.java: Util class to deal with proto objects

Steps:
1. Compile the employee.proto and building.proto files present in the ProtoAssignmentResources folder. The java classes 
   "EmployeeProto" and "BuildingProto" are present in the package "ProtoPackage".
2. Use Main class and provide input arguments: type, csvFilePath, serializedFileOutputPath where type is "Employee" or 
   "Building", csvFilePath is path of input file of Building or Employee Details and serializedFileOutputPath is the 
   output path where serialized protobuf data will saved in a file.

---

## Assignment 4

### Problem Statement

1. Create a Mapreduce job to use the above people and building data, load proto objects with those values and then write 
   it back to HBase. While doing this drill, update the employee.proto file to include cafeteria_code as a new field. 
   Leave the value blank for now. Build jars for the above proto project, this will be used for downstream assignments
2. Create a MR job using the above created table (employee and building). Read both the tables using their jars added to 
   the lib. Parse its value using proto objects. Join both the tables on the basis of building_code and enrich the cafteria_code 
   for each employee using the cafteria_code available in the building proto object and write it back to hbase table. 
   Construct a decent unique rowkey for denoting each row of value.
3. Create a proto attendance.proto with fields like employee_id, date and generate random entries for different dates 
   for each employee. For every building find the employee with the lowest attendance. 

### Solution classes

#### Part1:

- BulkLoadMain: Driver class for mapper which creates HFile.
  - args[0]: TableName 
  - args[1]: Input filepath in hdfs  
  - args[2]: Output filepath for the HFile
- BulkLoadMapper: Creates the HFile for bulk upload using the input hdfs file.
- LoadHFile: Driver class for which takes in HFile generated from BulkLoadMapper as input and performs bulk load into given HBase table.
  - args[0]: Table name where the bulk load should happen  
  - args[1]: Input filepath of the Hfile created from BulkLoadMain

Steps:

1. Create tables for Employee and Building details in HBase for inserting proto objects by mapreduce job in next step. "CreateTable" class can be used for creating tables.
2. Run the "BulkLoadMain" by providing 3 input arguments: args0 = Table name, args1 = Input file path in hdfs from which proto objects are created, args2 = HFile output path.
3. Run the "LoadHFile" by providing 2 arguments: args0 = Table name, args1 = HFile path generated in previous step.
4. Change the employee.proto file to include cafeteria field and compile the file.



#### Part2:

- JoinDriver: Driver class for which performs join on EmployeeProto and BuildingProto tables based on Building code.

Steps:

1. Run the JoinDriver class by providing the table names, cloumn family names and column name of the tables created earlier. This populates the cafeteria code for Employee HBase table by joining on Building Code with Building table.

#### Part3:

- EmployeeAttendanceDriver.java: Creates the AttendanceProto table in HBase and populates attendance data into it via Bulk Upload. 
  Finds the employee with the lowest attendance for each building and outputs it

Prerequisites: Assignment 4 Part 1 should be run prior to this.  
Command Line Arguments:  
args[0]: Location of the Employee.csv file in HDFS  
args[1]: HFile out path. Should not exist in HDFS prior to running this job  
args[2]: Output folder location where MR Job would output the employee with lowest attendance for each building  

---