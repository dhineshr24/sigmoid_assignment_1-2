package assignment_4.part1;

import protoPackage.AttendanceProto;
import protoPackage.BuildingProto;
import protoPackage.EmployeeProto;
import com.github.javafaker.Faker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class ProtoBufUtils {

    public EmployeeProto.EmployeeDetails.Floor getFloorEnum(String floorNo){
        EmployeeProto.EmployeeDetails.Floor floorValue = EmployeeProto.EmployeeDetails.Floor.First;
        switch(floorNo){
            case "First":
                floorValue = EmployeeProto.EmployeeDetails.Floor.First;
                break;
            case "Second":
                floorValue = EmployeeProto.EmployeeDetails.Floor.Second;
                break;
            case "Third":
                floorValue = EmployeeProto.EmployeeDetails.Floor.Third;
                break;
            case "Fourth":
                floorValue = EmployeeProto.EmployeeDetails.Floor.Fourth;
                break;
            case "Fifth":
                floorValue = EmployeeProto.EmployeeDetails.Floor.Fifth;
                break;
        }
        return floorValue;
    }

    public EmployeeProto.EmployeeDatabase getEmpDBObj(String[] record){
        String[] employeeRecord = record;
        //int numberOfColumns = employeeRecord.length;

        EmployeeProto.EmployeeDetails employeeProtoObj = EmployeeProto.EmployeeDetails.newBuilder()
                .setEmployeeId(employeeRecord[0])
                .setName(employeeRecord[1]).setBuildingCode(employeeRecord[2])
                .setFloorNo(getFloorEnum(employeeRecord[3]))
                .setSalary(Integer.parseInt(employeeRecord[4]))
                .setDepartment(employeeRecord[5])
                .build();

        EmployeeProto.EmployeeDatabase empDB = EmployeeProto.EmployeeDatabase.newBuilder()
                .addEmployee(employeeProtoObj).build();
        return empDB;
    }

    public BuildingProto.BuildingDatabase getBuildingDBObj(String[] record){
        String[] buildingRecord = record;
        //int numberOfColumns = employeeRecord.length;
        BuildingProto.BuildingDetails buildingProtoObj = BuildingProto.BuildingDetails.newBuilder()
                .setBuildingCode(buildingRecord[0])
                .setTotalFloors(Integer.parseInt(buildingRecord[1]))
                .setCompaniesInTheBuilding(buildingRecord[2])
                .setCafteriaCode(buildingRecord[3])
                .build();
        BuildingProto.BuildingDatabase buildingDB = BuildingProto.BuildingDatabase.newBuilder()
                .addBuilding(buildingProtoObj).build();
        return buildingDB;
    }

    public AttendanceProto.AttendanceDatabase getAttendanceDBObj(String[] record){
        //Record should come in the format: employee_id, day, month, year
        String[] attendanceRecord = record;
        Faker faker = new Faker();
        List<AttendanceProto.AttendanceDetails> attendanceDetailsList = new ArrayList<>();
        AttendanceProto.AttendanceDetails attendanceProtoObj;

        Random randomFrequencyGenerator = new Random();
        //Taking 15 days limit
        int totalDaysAttended = randomFrequencyGenerator.nextInt(16);
        for (int day=1; day<=totalDaysAttended; day++ ) {
            attendanceProtoObj = AttendanceProto.AttendanceDetails.newBuilder()
                    .setEmployeeId(attendanceRecord[0])
                    .setAttendanceDate(faker.date().between(new Date(2021,6,1), new Date(2021,6,30)).toString())
                    .build();
            attendanceDetailsList.add(attendanceProtoObj);
            }
        AttendanceProto.AttendanceDatabase attendanceDB = AttendanceProto.AttendanceDatabase.newBuilder().addAllAttendance(attendanceDetailsList).build();
        return attendanceDB;
    }
}
